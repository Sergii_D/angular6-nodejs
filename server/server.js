const express = require('express');
const morgan = require('morgan');
const cors = require('cors')
const dishRouter = require('./serverModules/dishes.router');
const promoRouter = require('./serverModules/promo.router');

const hostname = 'localhost';
const port = 3000;

const corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
}
  
const app = express();

app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));
app.use(cors(corsOptions));

app.use('/dishes',dishRouter);
app.use('/promo',promoRouter);


app.listen(port, hostname, function(){
  console.log(`Server running at http://${hostname}:${port}/`);
});


