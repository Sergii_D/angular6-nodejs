const express = require('express');
const bodyParser = require('body-parser');
const checkIfAuthenticated = require('./autentivation.module');

const promoRouter = express.Router(); 

promoRouter.use(bodyParser.json());

promoRouter.route('/')
    .all(function(req,res,next) {
        const token = req.headers.authorization

        if(checkIfAuthenticated(token)){
            res.writeHead(200, { 'Content-Type': 'application/json' });
            next();
        } else {
            res.status(401).send('error123');
            res.end();
        }

    })

    .get(function(req,res,next){
        res.end('{"text": "get promo"}');
    })

    .post(function(req, res, next){
        // res.send(201, req.body);
        res.end('{"text": "post promo"}');    
    })

    .delete(function(req, res, next){
        res.end('{"text": "Will send all the dishes to you!"}');
    });

promoRouter.route('/:promoId')
    .all(function(req,res,next) {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        next();
    })

    .get(function(req,res,next){
        res.end('Will send details of the dish: ' + req.params.dishId +' to you!');
    })

    .put(function(req, res, next){
        res.write('Updating the dish: ' + req.params.dishId + '\n');
        res.end('Will update the dish: ' + req.body.name + 
        ' with details: ' + req.body.description);
    })

    .delete(function(req, res, next){
            res.end('Deleting dish: ' + req.params.dishId);
    });

    
module.exports  = promoRouter;