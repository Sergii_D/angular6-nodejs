const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const checkIfAuthenticated = require('./autentivation.module');
const dishesRouter = express.Router(); 


const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "D@ny1iuk0501",
    database: "jester"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

dishesRouter.use(bodyParser.json());

dishesRouter.route('/')
    .all(function(req,res,next) {
        const token = req.headers.authorization

        if(checkIfAuthenticated(token)){
            res.writeHead(200, { 'Content-Type': 'application/json' });
            next();
        } else {
            res.status(401).send('error123');
            res.end();
        }

    })

    .get(function(req,res,next){
        con.query("SELECT * FROM Persons", function (err, result, fields) {
            if (err) throw err;
            console.log(result);
            res.end(JSON.stringify(result));
        });
    })

    .post(function(req, res, next){
        console.log(req.body.FirstName);
        let sql = `INSERT INTO Persons(LastName, FirstName, Age) VALUES (${req.body.LastName}, ${req.body.FirstName}, ${req.body.Age})`;
        con.query(sql, function (err, result, fields) {
            if (err) throw err;
            console.log(result);
            res.end(JSON.stringify(result));
        });

        res.end('{"text": "post dishes"}');    
    })

    .delete(function(req, res, next){
        res.end('{"text": "delete dishes"}');
    });

    dishesRouter.route('/:promoId')
    .all(function(req,res,next) {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        next();
    })

    .get(function(req,res,next){
        res.end('Will send details of the dish: ' + req.params.dishId +' to you!');
    })

    .put(function(req, res, next){
        res.write('Updating the dish: ' + req.params.dishId + '\n');
        res.end('Will update the dish: ' + req.body.name + 
        ' with details: ' + req.body.description);
    })

    .delete(function(req, res, next){
            res.end('Deleting dish: ' + req.params.dishId);
    });

    
    module.exports  = dishesRouter;