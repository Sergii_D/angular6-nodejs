import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getDishes(): Observable<any> {
    return this.http.get('http://localhost:3000/dishes');
  }

  addDish(): Observable<any> {
    return this.http.post('http://localhost:3000/dishes', {FirstName: Math.random(), LastName: Math.random(), Age: 10});
  }

  getPromo(): Observable<any> {
    return this.http.get('http://localhost:3000/promo');
  }
}
