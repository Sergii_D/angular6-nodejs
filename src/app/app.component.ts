import { Component } from '@angular/core';
import { DataService } from './_services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'nodeJs';

  constructor(private dataService: DataService) {}

  getDishes(): void {
    console.log(1);
    this.dataService.getDishes().subscribe(res => {
      console.log(res);
    })
  }

  addDish(): void {
    this.dataService.addDish().subscribe(res => {
      console.log(res);
    });
  }

  getPromo(): void {
    console.log(1);
    this.dataService.getPromo().subscribe(res => {
      console.log(res);
    })
  }
}
